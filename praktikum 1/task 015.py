# Task 015
# classes

class Car: #defined tha class
	def __init__(self, name, wheels): # defines a method
		"""
		This  method is ran on each instantiation of this class
		example, 'Car('hi')' will run this method
		"""
		self.name= name # assigns a property to the instance
		self.wheels = wheels

	def __str__(self):
		"""
		Gives the instance a name. Is called during 'print(instance)'
		"""
		return "{0} With {1} wheels".format(self.name, self.wheels)

	def print_wheels(self):
		print(self)
		print(self.wheels)

c = Car('firstcar', 18) # make an object from a class, ie Instantiation


print(c) # when print a instance, the __str__ method is called

c.print_wheels()
