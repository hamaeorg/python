# Task 022
# Unit test

import unittest

HELLO_WORLD = 'hello world'

class HelloTest(unittest.TestCase):
    def test_string_correct(self):
        print('test string ran')
        self.assertEqual(HELLO_WORLD, 'hello world')
