# Task 021
# make FizzBuzz testable and make unitests for it
import unittest


#from fizzbuzzer.fizzbuzzer.py import fizzbuzzer

def fizzbuzzer(i):
    if i % 15 == 0:
        result = 'FizzBuzz'
    elif i % 5 == 0:
        result = 'Buzz'
    elif i % 3 == 0:
        result = 'Fizz'
    else:
        result = i
    return result

if __name__ == "__main__":
    for i in range (1, 101):
        print(fizzbuzzer(i))

class FizzBuzzTestCase(unittest.TestCase):
    def test_fizz(self):
        result = str(fizzbuzzer(3))
        self.assertEqual(result, 'Fizz')
    def test_buzz(self):
        result = str(fizzbuzzer(5))
        self.assertEqual(result, 'Buzz')
    def test_fizzbuzz(self):
        result = str(fizzbuzzer(15))
        self.assertEqual(result, 'FizzBuzz')
    def test_neither(self):
        result = str(fizzbuzzer(1))
        self.assertEqual(result, '1')




