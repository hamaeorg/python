# Task 074
# Return the sum of ll arguments. Pass a list into the function

def my_sum_function(*args):
    return sum(args)
if __name__ == "__main__":
    print(my_sum_function(2, 3, 5))