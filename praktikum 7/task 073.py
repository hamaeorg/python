# Task 073
# Implement 'all' function with a for loop: my_all([True, True]) should return True

def my_all(list):
    result = True
    for index, value in enumerate(list):
        print index
        if not value:
            result = False
            break
    return result

if __name__ == "__main__":
    print(my_all([0, 2, False, 3, 4]))