# Sample 072
# Error handling
# ?isdigit

a = input('Type a number: ')
b = input('Type a number: ')

"""
if a.isdigit() and b.isdigit():
    int_A = int(a)
    int_B = int(b)
    print (int_A + int_B)
"""

try:
    int_A = int(a)
    int_B = int(b)
    print (int_A + int_B)
except ValueError:
    print('They need to be integers!')
