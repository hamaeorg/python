# Task 072
# Find if all elements are false: my_all([True, True]) should return True

def my_all(list):
    result = True
    for i in list:
        if not i:
            result = False
    return result

if __name__ == "__main__":
    print(my_all([True, True]))