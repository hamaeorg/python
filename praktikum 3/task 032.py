# Task 032
# Make a new csv file and save it, where tha age becomes year born
#?

import argparse
import csv

file = open('./names.csv')

TEMPLATE_STRING = "{name},{year_born}"

def render_row(row):
    name = row[1].strip() # remove new line in name
    age = row[7]
    year_born = 2017 - int(age)
    line_out = TEMPLATE_STRING.format(name=name, year_born=year_born)
    return line_out

def convert_age_in_csv(data_in, data_out):
    reader = csv.reader(data_in)
    writer = csv.writer(data_out)
    for row in reader:
        name = row[0].strip
        age = int(row[1])
        year_born = 2017 - age
        writer.writerow([name, year_born])

for row in file:
    line_out = render_row(row)
    print(line_out)
    #print(row)

