# Sample 033

a = {'Estonian': 'Estonian', 'America': 'English', 'Russia': 'Russian', 'Australia': 'English'} #dictionary

print(a)
print

print(a['America'])

a['America'] = 'American'
print(a['America'])

a['America'] = 'Super ' + a['America']
print(a['America'])
print

print(1 == 1)

print(1 != 1)

print(1 != 2)
print

print(list(a))
print

print('e' in 'Hello')
print('a' in 'Hello')
print(False and True)
