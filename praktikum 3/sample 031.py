# Sample 031

a = '1'
print(len(a))

print(a == a)

print(a == '1')

print(a is '1') #checks if the objet is the same

print(id(a))

print(id('1'))

b = '1'

print(a is b)

a = [1] # a list with one objet '1'
b = [1]

print(a is b)  # a and b are not the same object

a = b

print(a is b)