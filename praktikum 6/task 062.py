# Task 062
# Write a function that makes all characters in the values of a dict uppercase. Call it with the following dict and print the result: {'cat': 'kass, 'dog': 'koer'}

MY_DICT = {'cat': 'kass', 'dog': 'koer'}


def to_upper_case(dictionary):
    new_dict = {}
    for k, v in dictionary.items(): # k - key; v - value
        new_dict[k] = v.upper()
    return new_dict

if __name__ == "__main__":
    print(to_upper_case(MY_DICT))