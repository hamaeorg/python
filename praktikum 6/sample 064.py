# Sample 064

import matplotlib.pyplot as plt
import pandas as pd
import time

btc_price = pd.read_csv('market-price.csv', names=['datetime', 'usd'], parse_dates=[0])

print type(btc_price)

# print btc_price.datetime
# print btc_price.usd

x = btc_price.datetime
y = btc_price.usd

print x
print y
plt.plot(x, y)

plt.xlabel('Date')
plt.ylabel('USD')
plt.title('BTC to USD')
#time.sleep(1)
plt.grid(True)
plt.show()

