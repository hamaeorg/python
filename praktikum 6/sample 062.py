# Sample 062

import numpy as np

my_array = np.array([1,2,3])

print my_array[1]
print my_array[::-1]
print my_array[::-1][0]
print my_array

print
print [x**2 for x in my_array]

print my_array ** 2

print my_array.__pow__(2)
print my_array.__add__(2)

print

a = np.arange(9)
print a

b = a.reshape(3,3)
print b

print

c = np.array([[2, 0],[1,0]])
print c




