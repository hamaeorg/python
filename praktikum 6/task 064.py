# Task 064
# write a function that sorts numbers in a list by the smallest digit of the integer

MY_LIST = [11, 10, 9, 177, 199]

def rank_integer_by_last_digit(n):
    return str(n)[::-1]

def sort_function(list):
    return sorted(MY_LIST, key=rank_integer_by_last_digit)

if __name__ == "__main__":
    print(sort_function(MY_LIST))


