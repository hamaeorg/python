# Task 065
# solve x, y and z
"""
x + y + z = 6
2y + 5z = -4
2x + 5y - z = 27
"""

import numpy as np

A = np.array([[1,1,1],[0,2,5],[2,5,-1]])
print A

B = np.array([[6],[-4],[27]])
print B

A_inv = np.linalg.inv(A)
print A_inv


print A_inv.dot(B)

