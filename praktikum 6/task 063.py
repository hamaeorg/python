# task 063
# write a function that filters out numbers divisible by 3 and 5. call it with range(0,30)
#?

def filter(l):
    new_list = []
    for i in l:
        if not (i % 3 == 0 or i % 5 == 0):
            new_list.append(i)
    return new_list



def filter_functional(n):
    if not (n % 3 == 0 or n % 5 == 0):
        return True
    else:
        return False

def functional_filter_numbers(l):
    result = list(filter(filter_functional, l))
    return result



if __name__ == "__main__":
    print(filter(range(0, 30)))
    print(functional_filter_numbers(range(0, 30)))





