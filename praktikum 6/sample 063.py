# Sample 063
# bitcoin
import pandas as pd
from datetime import datetime
from datetime import timedelta

btc_price = pd.read_csv('market-price.csv', names=['datetime', 'usd'], parse_dates=[0])

#print btc_price

#print type(btc_price)

#print btc_price['usd']

#print btc_price.usd.mean()

print btc_price[btc_price.usd > 1000] #days, when ptc price is above 1000 dollars

print len(btc_price[btc_price.usd > 1000]) #how many days are there, where btc price is above 1000 dollars

print btc_price[btc_price.usd > 1000].count()

d = datetime.utcnow()
print d

print timedelta(days=365/2)

day = datetime.utcnow() - timedelta(days=365/2)
print day

print btc_price.usd[182]

print datetime.utcnow() - timedelta(days=600)
print datetime.utcnow() - timedelta(days=600 + 400)
print
print datetime.datetime.now() - timedelta(500)