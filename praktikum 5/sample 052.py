# Sample 052
#?

import json
import requests
import unittest
from unittest import TestCase


MOCK_JSON_DATA = '''
{"time":{"updated":"Mar 14, 2017 19:57:00 UTC","updatedISO":"2017-03-14T19:57:00+00:00","updateduk":"Mar 14, 2017 at 19:57 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"1,250.1550","description":"United States Dollar","rate_float":1250.155},"GBP":{"code":"GBP","symbol":"&pound;","rate":"1,027.9187","description":"British Pound Sterling","rate_float":1027.9187},"EUR":{"code":"EUR","symbol":"&euro;","rate":"1,178.1661","description":"Euro","rate_float":1178.1661}}}
'''

class MockResponse:
    def json(self):
        json_dict = json.loads(MOCK_JSON_DATA)
        return json_dict

def mock_request_function(url):
    response = MockResponse
    return response

def get_btc_price(request_function=requests.get):
    r = request_function('http://api.coindesk.com/v1/bpi/currentprice.json')
    data = r.json()
    price = data['bpi']['EUR']['rate_float']
    return price

class BtcPriceTestCase(TestCase):
    def test_price_is_floating_point(self):
        price = get_btc_price(request_function=mock_request_function())
        float(price)

if __name__ == '__main__':
    print(get_btc_price(request_function=mock_request_function()))
