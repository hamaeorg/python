# Sample 054

import datetime

now = datetime.datetime.utcnow()

print now
print now.year
print now.day
print now.month


print now.isoformat()

print now.strftime('%y')
print now.strftime('%Y')

print "{} {}".format(now.day, now.month)

print datetime.datetime.fromtimestamp(1489517406.50323)