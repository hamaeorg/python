# Sample 058
# Lambda example

def get_third_element_of_tuple(student):
    return student [2]

lambda student: student[2]

student_tuples = [('john', 'A', 15), ('jane', 'B', 12), ('dave', 'B', 10)]

sorted_students = sorted(student_tuples, key=lambda student: student[2])
# sorted_students = sorted(student_tuples, key=get_third_element_of_tuple) # same

print(sorted_students)

a = lambda b: b + 1
print a(3)