# Sample 051
# unitest bitcon price

import unittest
import requests
from unittest import TestCase

import requests

def get_btc_price():
    r = requests.get('http://api.coindesk.com/v1/bpi/currentprice.json')
    r.content
    data = r.json()
    return data['bpi']['EUR']['rate_float']

class BtcPriceTestCase(TestCase):
    def test_price_is_floating_point(self):
        price = get_btc_price()
        float(price)

if __name__ == '__main__':
    print(get_btc_price())
