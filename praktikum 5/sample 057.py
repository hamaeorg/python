# Sample 057

class Vehicle(object):
    steering_wheel_count = 1

class LearningVehicleMixin:
    steering_wheel_count = 2

class BreakMixin:
    def break_(self):
        print('STOP!!!')

class Car(Vehicle, BreakMixin):

    def __init__(self, wheels=4):
        self.wheels = wheels #instance/object variable

    @staticmethod
    def drive(cls):
        print('Gotta go fast!')

    @classmethod
    def get_steering_wheel_count(cls):
        return cls.steering_wheel_count

class Bike(Vehicle, BreakMixin):
    pass

class LearningCar(LearningVehicleMixin, Car):
    pass

"""
class LearningCar(Car, LearningVehicleMixin):
    pass
"""
print(LearningCar.steering_wheel_count)