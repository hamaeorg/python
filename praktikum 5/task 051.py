# Task 051
# Print the titles of all the frontpage articles on reddit, with the time in normal format (i.e 11:23) next to it

import json
import datetime
import requests

def format_article(article):
    timestamp = article['data']['created_utc']
    d = datetime.datetime.fromtimestamp(timestamp)
    time_string = d.strftime('%I:%M')

    title = article['data']['title']
    result = "{time} {titel}".format(time=time_string, titel=title)
    return result

def main():
    json_file = open('front_page.json')
    data = json.load(json_file)
    json_file.close()
    articles = data['data']['children']

    for article in articles:
        result = format_article(article)
        print (result)

if __name__ == "__main__":
    main()




