# sample 056

class Car:
    steering_wheel_count = 1 # class variable

    def __init__(self, wheels=4):
        self.wheels = wheels #instance/object variable

    @staticmethod
    def drive(cls):
        print('Gotta go fast!')

    @classmethod
    def get_steering_wheel_count(cls):
        return cls.steering_wheel_count


car_instance = Car()
car_instance.drive()
print(car_instance.get_steering_wheel_count())