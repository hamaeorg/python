The project is located in Project folder.

The program starts by running stock_trend_analysis.py.

stock_trend_analysis.py is  program that genertes a technical analysis trading trend graph based on a selected stock ticker symbol and timeframe chosen by the user. 
The program generates two exponential moving average lists(EMA's). First one is the EMA of last 200 days and the second one is the EMA of last 50 days. 
If the 50'th day EMA is the average value of the elements 0-50, then the 51'th day EMA is the average value of the elements 1-51. A graph is generated based on these values and it shows the currend price trend of the stock. The meeting points of the two lines on the graph are the teoretical buying or selling points of the stock. (But it should only be one part on the stock buying/selling decision. This method doesn't inculde any news or annual reports or any other criterias, that should be considered before making a buying/selling decision)

Using the program:
the first thing the program does when it starts is ask for user input:
"enter the ticker symbol you're interested in: " - you can search the ticker symbol of the stock you're interested in from finance.yahoo.com . For example "Tallinna Kaubamaja" stock ticker symbol is TKM1T.TL. you should enter the ticker symbol like this: TKM1T.TL
The seocond input asks for the time day count, when the stock price is examined. The user input should be entered in days. The period is then calculated from the  amount of days back up to today.


Motivation:
The motivation behind this program was, that there was no automated way to view baltic stock price trends, without buying the bloomberg station, that costs about 1000 dollar a month. There is was a way to view the same information about the stocks listed on bigger stock markets, but not in baltic stock market. 
The original purpose was to create a program that can analyse the price trends of stocks in baltic stock market. But since this program gets its information from finance.yahoo database you can not only ask for information about baltic stock market, but also about stocks all over the world and about some other financial instrument traded on the stockmarket all over the world.

For example:

stock - Apple Inc. (AAPL)

index - Dow Jones Industrial Average (^DJI)

ETF - SPDR Gold Shares (GLD)
