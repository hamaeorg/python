import csv
import requests
import datetime
from dateutil.parser import parse

class Stock:

    def __init__(self, name, time):
        self.my_list = self.read_csv(name, time)
        self.t = time
        self.n = name


    def __str__(self):
        return self.n


    def price(self):
        list = self.my_list
        output = [[0]*2 for e in range(self.t)]
        i = -1
        for x in range(0, self.t):
            if x >= (len(list) - 1):
                output[x][0] = (output[i][0] - datetime.timedelta(days=1))
                i += 1
                output[x][1] = 0
            else:
                output[x][0] = parse(list[x + 1][0])
                output[x][1] = list[x + 1][1]
                i += 1
        return output


    def read_csv(self, name, time):
        today = datetime.datetime.now()

        start_date = today - datetime.timedelta(days=((time * 1.6) + 300))

        end_time = '&a=' + str(today.month - 1) + '&b=' + str(today.day) + '&c=' + str(today.year)
        start_time = '&a=' + str(start_date.month - 1) + '&b=' + str(start_date.day) + '&c=' + str(start_date.year)

        CSV_URL = 'http://ichart.finance.yahoo.com/table.csv?s=' + name + start_time + end_time + '&g=d&ignore=.csv'

        with requests.Session() as s:
            download = s.get(CSV_URL)

            decoded_content = download.content.decode('utf-8')

            cr = csv.reader(decoded_content.splitlines(), delimiter=',')
            my_list = list(cr)

            return my_list


    def exponential_moving_average(self, number):

        list = self.my_list
        output = [[0]*2 for e in range(self.t)]
        j = -1
        for x in range(0, self.t):
            summa = 0.0
            for i in range(x + 1,  x + number + 1):
                if i>= len(list):
                    summa += 0
                else:
                    summa += float(list[i][1])
            if x >= (len(list) - 1):
                output[x][0] = (output[j][0] - datetime.timedelta(days=1))
                j += 1
                output[x][1] = 0
            else:
                output[x][0] = parse(list[x + 1][0])
                j += 1

            output[x][1] = ('% 10.5f' % (summa/number))
        return output