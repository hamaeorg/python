from stock import Stock
import matplotlib.pyplot as plt
from unittest import TestCase

class trend_analysis_test(TestCase):
    def test_exponential_moving_average(self):
        stock = Stock('LHV1T.TL', 700)
        print('test ran')
        list_length = len(stock.price())
        self.assertEqual(list_length, 700)


def graph_drawer(name, time):
    stock = Stock(name, time)
    price = stock.price()
    EMA_1 = stock.exponential_moving_average(200)
    EMA_2 = stock.exponential_moving_average(50)

    price_line = [row[0] for row in price]
    price_time = [row[1] for row in price]
    EMA_1_line = [row[0] for row in EMA_1]
    EMA_1_time = [row[1] for row in EMA_1]
    EMA_2_line = [row[0] for row in EMA_2]
    EMA_2_time = [row[1] for row in EMA_2]

    plt.plot(price_line, price_time, label='Price', color='0.75')
    plt.plot(EMA_1_line, EMA_1_time, label='EMA(200)')
    plt.plot(EMA_2_line, EMA_2_time, label='EMA(50)')

    plt.legend(loc=9, ncol=2)
    plt.xlabel('Date')
    plt.ylabel('Currency')
    plt.title(name)
    plt.grid(True)
    plt.show()


def user_interface():
    name = raw_input("enter the ticker symbol you're interested in: ")
    time = input("enter the time period you're interested in(in days): ")
    graph_drawer(name, time)

if __name__ == "__main__":
    user_interface()


